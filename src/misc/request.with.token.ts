import { JwtDataDto } from "src/dtos/administrator/auth/jwt.data.dto";

declare module 'express'{
    interface Request {
        token: JwtDataDto;
    }
}