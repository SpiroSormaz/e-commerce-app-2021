import { Controller, UseGuards } from "@nestjs/common";
import { Crud } from "@nestjsx/crud";
import { Category } from "src/entities/category.entity";
import { AllowToRoles } from "src/misc/allow.ro.roles.descriptor";
import { RoleCheckedGaurd } from "src/misc/role.checker.guard";
import { CategoryService } from "src/services/category/category.service";

@Controller('api/category')
@Crud({
    model:{
        type:Category
    },
    params:{
        id:{
            field:'categoryId',
            type:'number',
            primary:true
        }
    },
    query:{
        join:{
            categories:{
                eager:true
            },
            features:{
                eager:true
            },  
            parentCategory:{
                eager:false
            },
          
            articles:{
                eager:false
            }
        }
    },
    routes:{
        only:[
            "createOneBase",
            "createManyBase",
            "getManyBase",
            "getOneBase",
            "updateOneBase",
        ],
        createOneBase:{
            decorators:[
                UseGuards(RoleCheckedGaurd),
                AllowToRoles('administrator')
            ]
        },
        createManyBase:{
            decorators:[
                UseGuards(RoleCheckedGaurd),
                AllowToRoles('administrator')
            ]

        },
        getManyBase:{
            decorators:[
                UseGuards(RoleCheckedGaurd),
                AllowToRoles('administrator', 'user')
            ]
        },
        getOneBase:{
            decorators:[
                UseGuards(RoleCheckedGaurd),
                AllowToRoles('administrator', 'user')
            ]
        },
        updateOneBase:{
            decorators:[
                UseGuards(RoleCheckedGaurd),
                AllowToRoles('administrator')
            ]
        }
    }
})
export class CategoryController {
    constructor(public service: CategoryService){}
}