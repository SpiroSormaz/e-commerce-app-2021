import { Controller, Get, Param, UseGuards } from "@nestjs/common";
import { Crud } from "@nestjsx/crud";
import { DistinctFeatureValuesDto } from "src/dtos/feature/distinct.feature.values.dto";
import { Feature } from "src/entities/feature.entity";
import { AllowToRoles } from "src/misc/allow.ro.roles.descriptor";
import { RoleCheckedGaurd } from "src/misc/role.checker.guard";
import { FeatureService } from "src/services/feature/feature.service";

@Controller('api/feature')
@Crud({
    model:{
        type:Feature
    },
    params:{
        id:{
            field:'featureId',
            type:'number',
            primary:true
        }
    },
    query:{
        join:{
            articleFeatures:{
                eager:false
            },
            category:{
                eager:true
            },  
            articles:{
                eager:false
            },
        }
    },
    routes:{
        only:[
            "createOneBase",
            "createManyBase",
            "getManyBase",
            "getOneBase",
            "updateOneBase",
        ],
        createOneBase:{
            decorators:[
                UseGuards(RoleCheckedGaurd),
                AllowToRoles('administrator')
            ]
        },
        createManyBase:{
            decorators:[
                UseGuards(RoleCheckedGaurd),
                AllowToRoles('administrator')
            ]

        },
        getManyBase:{
            decorators:[
                UseGuards(RoleCheckedGaurd),
                AllowToRoles('administrator', 'user')
            ]
        },
        getOneBase:{
            decorators:[
                UseGuards(RoleCheckedGaurd),
                AllowToRoles('administrator', 'user')
            ]
        },
        updateOneBase:{
            decorators:[
                UseGuards(RoleCheckedGaurd),
                AllowToRoles('administrator')
            ]
        }
    }
})
export class FeatureController {
    constructor(public service:FeatureService){}

    @Get('values/:categoryId')
    @UseGuards(RoleCheckedGaurd)
    @AllowToRoles("administrator", "user")
    async getDistinctValuesByCategoryId(@Param('categoryId') categoryId: number) :Promise<DistinctFeatureValuesDto>{
        return await   this.service.getDistinctValuesByCategoryId(categoryId);
    }

}