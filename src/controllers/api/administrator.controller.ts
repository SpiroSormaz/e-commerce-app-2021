import { Body, Controller, Get, Param, Post, Put, SetMetadata, UseGuards } from "@nestjs/common";
import { ApiResponse } from "src/misc/api.response.class";
import { Administrator } from "src/entities/administrator.entity";
import { AddAdministratorDto } from "../../dtos/administrator/add.administrator.dto";
import { EditAdministratorDto } from "../../dtos/administrator/edit.administrator.dto";
import { AdministratorService } from "../../services/administrator/administrator.service";
import { AllowToRoles } from "src/misc/allow.ro.roles.descriptor";
import { RoleCheckedGaurd } from "src/misc/role.checker.guard";

@Controller('api/administrator')
export class AdministratorController{
    constructor(
        private administratorService:AdministratorService
    ){}
   
    @Get()
    @UseGuards(RoleCheckedGaurd)
    @AllowToRoles("administrator")
    getAllAdmins():Promise<Administrator[]>{
        return this.administratorService.getAll();
    }

    @Get(':id')
    @UseGuards(RoleCheckedGaurd)
    @AllowToRoles("administrator")
    getById(@Param('id')administratorId:number):Promise<Administrator | ApiResponse>{
        return this.administratorService.getById(administratorId);
    }

    @Put()
    @UseGuards(RoleCheckedGaurd)
    @AllowToRoles("administrator")
    add(@Body()data:AddAdministratorDto):Promise<Administrator | ApiResponse>{
         return this.administratorService.add(data) 
    }

    @Post(':id')
    @UseGuards(RoleCheckedGaurd)
    @AllowToRoles("administrator")
    edit(@Param('id')id:number,@Body()data: EditAdministratorDto):Promise<Administrator | ApiResponse>{
         return this.administratorService.editById(id, data)

    }
}