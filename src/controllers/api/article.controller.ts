import { Body, Controller, Delete, Param, Patch, Post, Req, UploadedFile, UseGuards, UseInterceptors } from "@nestjs/common";
import { FileInterceptor} from "@nestjs/platform-express";
import { Crud } from "@nestjsx/crud";
import { StorageConfig } from "config/storage.config";
import { Article } from "src/entities/article.entity";
import { AddArticleDto } from "src/dtos/article/add.article.dto";
import { ArticleService } from "src/services/article/article.service";
import {diskStorage} from "multer";
import { PhotoService } from "src/services/photo/photo.service";
import { Photo } from "src/entities/photo.entity";
import { ApiResponse } from "src/misc/api.response.class";
import * as fileType from 'file-type';
import * as fs from 'fs';
import * as sharp from 'sharp';
import { captureRejectionSymbol } from "events";
import { EditArticleDto } from "src/dtos/article/edit.article.dto";
import { AllowToRoles } from "src/misc/allow.ro.roles.descriptor";
import { RoleCheckedGaurd } from "src/misc/role.checker.guard";
import { ArticleSearchDto } from "src/dtos/article/article.search.dto";


@Controller('api/article')
@Crud({
    model:{
        type:Article
    },
    params:{
        id:{
            field:'articleId',
            type:'number',
            primary:true
        }
    },
    query:{
        join:{
            category:{
                eager:true
            },
            photos:{
                eager:true
            },
            articlePrices:{
                eager:true
            },
            articleFeatures:{
                eager:true
            },
            features:{
                eager:true
            }
            
     
        }
    },

    routes:{
        only:[
            'getOneBase',
            'getManyBase'
        ],

        getOneBase:{
            decorators:[
                UseGuards(RoleCheckedGaurd),
                AllowToRoles('administrator',  'user')

            ]
        },
        getManyBase:{
            decorators:[
                UseGuards(RoleCheckedGaurd),
                AllowToRoles('administrator',  'user')

            ]

        }
    },
})
export class ArticleController {
    constructor(
        public service: ArticleService,
        public photoService:PhotoService
        
        ){}

    @Post()
    @UseGuards(RoleCheckedGaurd)
    @AllowToRoles("administrator")
    createFullArticle(@Body()data:AddArticleDto){
        return this.service.createFullArticle(data)

    }
    
    @Patch(':id')
    @UseGuards(RoleCheckedGaurd)
    @AllowToRoles("administrator")
    editFullArticle(@Param('id') id: number, @Body() data: EditArticleDto){
        return this.service.editFullArticle(id, data);
    }
     
    @Post(':id/uploadPhoto') // http://localhost:3000/api/article/:id/uploadPhoto/
    @UseGuards(RoleCheckedGaurd)
    @AllowToRoles("administrator")
    @UseInterceptors(
        FileInterceptor('photo',{
           storage:diskStorage({
               destination:StorageConfig.photo.destination,
               filename: (req, file, callback) => {

                let original:string = file.originalname;

                let normalized = original.replace(/\s+/g, '-');
                let current = new Date();
                let datePart = '';
                datePart += current.getFullYear();
                datePart += (current.getMonth() + 1).toString();
                datePart += current.getDate().toString();


                let randomPart = new Array(10).fill(0)
                
                .map(e => (Math.random()* 9).toFixed(0).toString()).join('')
                let fileName = datePart + '-' + randomPart + '-' + normalized;

                callback(null, fileName)
               }
           }),

           fileFilter:(req, file , callback) => {
               // 1.Check extension

               if(!file.originalname.toLowerCase().match(/\.(jpg|png)$/)){
                req.fileFilterError = "Bad file extension";
                callback(null,false);
                return;
               }

               // 2.Check mimetype

               if(!(file.mimetype.includes('jpeg') || file.mimetype.includes("png"))){
                req.fileFilterError = "Bad file content";
                callback(null,false);
                   return;
               }

                callback(null, true);
           },

            limits:{
                files:1,
                fileSize:StorageConfig.photo.maxSize
            }
        })
    ) 
    async uploadPhoto(@Param('id') articleId: number,@UploadedFile() photo,
    @Req()req
    ):Promise<ApiResponse | Photo>{
    
        if(req.fileFilterError){
            return new ApiResponse("error", -4002,req.fileFilterError);
        }

          if(!photo){
            return new ApiResponse("error", -4002,"File not uploaded");

          }

          const fileTypeResult = await fileType.fromFile(photo.path);

          if(!fileTypeResult){
            fs.unlinkSync(photo.path)
            return new ApiResponse("error", -4002,"Cannot detect file type!");
          }

          const realMimeType = fileTypeResult.mime;

          if(!(realMimeType.includes('jpeg') || realMimeType.includes("png"))){
            fs.unlinkSync(photo.path)
            return new ApiResponse("error", -4002,"Bad file content type!");

          }
          
          await this.createResizeImg(photo, StorageConfig.photo.resize.thumb)
          await this.createResizeImg(photo, StorageConfig.photo.resize.small)

         const newPhoto: Photo = new Photo();
         newPhoto.articleId = articleId;
         newPhoto.imagePath = photo.filename;

         const savedPhoto = await this.photoService.add(newPhoto);
         if(!savedPhoto){
             return new ApiResponse("error", -4001,"photo is not uploaded");

         }

         return savedPhoto;
     }


     async createThumb(photo){
        await this.createResizeImg(photo, StorageConfig.photo.resize.thumb)

     }

     async createSmallImage(photo){
       await this.createResizeImg(photo, StorageConfig.photo.resize.small)

     }


     async createResizeImg(photo, resizeSettings){

        const originalFilePath = photo.path;
        const fileName = photo.filename;

        const destinationFilePath = StorageConfig.photo.destination + 
                                    resizeSettings.small.directory + 
                                    fileName;

        sharp(originalFilePath)
           .resize({
               fit: 'cover',
               width:resizeSettings.width,
               height:resizeSettings.height,
               background:{
                   r: 255, g: 255, b :255, alpha: 0.0
               }
           })
            .toFile(destinationFilePath);
     }

     @Delete(':articleId/deletePhoto/:photoId/')
     @UseGuards(RoleCheckedGaurd)
     @AllowToRoles("administrator")
     public async deletePhoto(@Param('articleId') articleid: number,
     @Param('photoId') photoId: number){
         
        const photo = await this.photoService.findOne({
            articleId: articleid,
            photoId: photoId
        });

         if(!photo){
             return new ApiResponse('error',-4004,'Photo not found')
         }
          try{

         fs.unlinkSync(StorageConfig.photo.destination + photo.imagePath);
         fs.unlinkSync(StorageConfig.photo.destination + StorageConfig.photo.resize.thumb.directory + photo.imagePath);
         fs.unlinkSync(StorageConfig.photo.destination + StorageConfig.photo.resize.small.directory + photo.imagePath);
          }catch(e){}
        const deleteResult = await this.photoService.deleteById(photo.photoId);
        


        if(deleteResult.affected === 0){
            return new ApiResponse('error', -4004, 'Photo not found')

        }

        return new ApiResponse('ok', 0, 'One photo deleted');
   }

   @Post('search')
   @UseGuards(RoleCheckedGaurd)
   @AllowToRoles("administrator", "user")
  async  search(@Body() data: ArticleSearchDto): Promise<Article[] | ApiResponse>{
       return await this.service.search(data);
   }
}