export class DistinctFeatureValuesDto{
    features: {
        featureId: number;
        name: string;
        values: string[];
    }[];
}