import { Injectable } from "@nestjs/common";
import { Order } from "src/entities/order.entity";
import {MailerService} from '@nestjs-modules/mailer'
import { MailerConfig } from "config/mail.config";
import { CartArticle } from "src/entities/cart-article.entity";


@Injectable()
export class OrderMailer {
    constructor(private readonly mailService: MailerService){}
    
    async sendOrderEmail(order: Order){
       await  this.mailService.sendMail({
           to: order.cart.user.email,
           bcc: MailerConfig.orderNotificationMail,
           subject: 'Order details',
           encoding: 'UTF-8',
           replyTo : 'no-replay@domain.com',
           html: this.makeOrderHtml(order),
        });

    }

   private  makeOrderHtml(order: Order): string{

        let suma  = order.cart.cartArticles.reduce((sum, currentCartArticle: CartArticle) =>{
             return sum + currentCartArticle.quantity * currentCartArticle.article.articlePrices[ currentCartArticle.article.articlePrices.length-1].price
        }, 0)
        return `<p>Thanks for order</p>
                <p>Order details</p>
                <ul>
                ${ order.cart.cartArticles.map((cartArticle: CartArticle) =>{
                  return `<li>
                   ${ cartArticle.article.name } x 
                   ${ cartArticle.quantity }
                  </li>`
                }).join("")}
                </ul>
                <p>Ukupan iznos je : ${ suma.toFixed(2) } EUR.</p>        `
    }
}