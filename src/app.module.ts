import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DatabaseConfiguration } from '../config/database.configuration';
import { Administrator } from './entities/administrator.entity';
import { ArticleFeature } from './entities/article-feature.entity';
import { ArticlePrice } from './entities/article-price.entity';
import { Article } from './entities/article.entity';
import { CartArticle } from './entities/cart-article.entity';
import { Cart } from './entities/cart.entity';
import { Category } from './entities/category.entity';
import { Feature } from './entities/feature.entity';
import { Order } from './entities/order.entity';
import { Photo } from './entities/photo.entity';
import { User } from './entities/user.entity';
import { AdministratorController } from './controllers/api/administrator.controller';
import { ArticleController } from './controllers/api/article.controller';
import { AuthController } from './controllers/api/auth.controller';
import { CategoryController } from './controllers/api/category.controller';
import { AppController } from './controllers/app.controller';
import { AuthMiddleware } from './controllers/middlewares/auth.middleware';
import { AdministratorService } from './services/administrator/administrator.service';
import { ArticleService } from './services/article/article.service';
import { CategoryService } from './services/category/category.service';
import { PhotoService } from './services/photo/photo.service';
import { FeatureService } from './services/feature/feature.service';
import { FeatureController } from './controllers/api/feature.controller';
import { UserService } from './services/user/user.service';
import { CartService } from './services/cart/cart.service';
import { userCartController } from './controllers/api/user.cart.controller';
import { OrderService } from './services/order/order.service';
import {MailerModule} from '@nestjs-modules/mailer'
import { MailerConfig } from 'config/mail.config';
import { OrderMailer } from './services/order/order.mailer.service';
import { UserToken } from './entities/user-token.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: DatabaseConfiguration.hostname,
      port:3306,
      username: DatabaseConfiguration.username,
      password: DatabaseConfiguration.password,
      database: DatabaseConfiguration.database,
      entities: [
        Administrator,
        ArticleFeature,
        ArticlePrice,
        Article,
        CartArticle,
        Cart,
        Category,
        Feature,
        Order,
        Photo,
        User,
        UserToken,
      
      ]
    }),
    TypeOrmModule.forFeature([
      Administrator,
      ArticleFeature,
      ArticlePrice,
      Article,
      CartArticle,
      Cart,
      Category,
      Feature,
      Order,
      Photo,
      User,
      UserToken,
    ]),
    MailerModule.forRoot({
      transport: 'smtps://' + MailerConfig.username + 
                        ':' + MailerConfig.password + 
                        '@' + MailerConfig.hostname,
      defaults:{
        from: MailerConfig.senderEmail,

     }
    })
  ],
  controllers: [
    AppController, 
    AdministratorController,
    CategoryController,
    ArticleController,
    AuthController,
    FeatureController,
    userCartController,

  ],
  providers: [
    AdministratorService,
    CategoryService,
    ArticleService,
    PhotoService,
    FeatureService,
    UserService,
    CartService,
    OrderService,
    OrderMailer
    ],
    exports:[
      AdministratorService,
      UserService,
    ]
})
export class AppModule implements NestModule{
    configure(consumer: MiddlewareConsumer){
       consumer.apply(AuthMiddleware)
       .exclude('auth/*')
       .forRoutes("api/*")
    }
  }

